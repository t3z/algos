﻿using System.Collections.Generic;
using AlgosAndDataStructures;
using FluentAssertions;
using Xunit;

namespace UnitTests
{
    public class BinaryTreeEnumeratorTests
    {
        public BinaryTreeEnumerator<int> Sut { get; set; }

        public BinaryTreeEnumeratorTests()
        {
            Sut = new BinaryTreeEnumerator<int>();
        }

        [Fact]
        public void EnumeratorShouldNotBeNull()
        {
            Sut.Should().NotBeNull();
        }

        [Fact]
        public void EnumeratorShouldImplementIEnumerator()
        {
            Sut.Should().BeAssignableTo<IEnumerator<int>>();
        }

        [Fact]
        public void InOrderTraversal_OnNodeWithoutChilds_ShouldSetValueAsCurrent()
        {
            var expectation = 5;
            Sut = new BinaryTreeEnumerator<int>(new BinaryTreeNode<int>(expectation));
            Sut.MoveNext();

            Sut.Current.ShouldBeEquivalentTo(expectation);
        }

        [Fact]
        public void InOrderTraversal_OnRootWithChilds_ShouldSetLeftValueAsCurrent()
        {
            var expectation = 5;
            var root = new BinaryTreeNode<int>(expectation);
            root.Left = new BinaryTreeNode<int>(3);
            Sut = new BinaryTreeEnumerator<int>(root);
            Sut.MoveNext();

            Sut.Current.ShouldBeEquivalentTo(root.Left.Value);
        }

        [Fact]
        public void InOrderTraversal_OnRootWithChilds_ShouldSetLeftmostValueAsCurrent()
        {
            var expectation = 5;
            var root = new BinaryTreeNode<int>(expectation);
            root.Left = new BinaryTreeNode<int>(3);
            root.Left.Left = new BinaryTreeNode<int>(1);
            Sut = new BinaryTreeEnumerator<int>(root);
            Sut.MoveNext();

            Sut.Current.ShouldBeEquivalentTo(root.Left.Left.Value);
        }

        [Fact]
        public void MoveNext_OnRootWithChild_ShouldReturnTrue()
        {
            var root = new BinaryTreeNode<int>(5);
            root.Left = new BinaryTreeNode<int>(4);
            Sut = new BinaryTreeEnumerator<int>(root);
            Sut.MoveNext().Should().BeTrue();
        }

        [Fact]
        public void MoveNext_OnRootQWithOneChild_ShouldChangeCurrentToRoot()
        {
            var root = new BinaryTreeNode<int>(5) { Left = new BinaryTreeNode<int>(4) };
            Sut = new BinaryTreeEnumerator<int>(root);
            Sut.MoveNext();
            Sut.MoveNext();
            Sut.current.ShouldBeEquivalentTo(root);
        }

        [Fact]
        public void MoveNext_OnRootHavingChilds_ShouldChangeCurrent()
        {
            var root = new BinaryTreeNode<int>(5) { Left = new BinaryTreeNode<int>(4) };
            Sut = new BinaryTreeEnumerator<int>(root);
            Sut.MoveNext();
            Sut.MoveNext();
            Sut.current.ShouldBeEquivalentTo(root);
        }

        [Fact]
        public void MoveNext_OnInitialization_ShouldReturnFalse()
        {
            Sut.MoveNext().Should().BeFalse();
        }

        [Fact]
        public void ShouldHaveInOrderTraversalMode()
        {
            Sut.TraversalMode.ShouldBeEquivalentTo(TraversalMode.InOrder);
        }

        [Fact]
        public void InOrderTraversal_OnLeftAndRightChildsSet_ShouldPickLeftChildFirst()
        {
            var root = new BinaryTreeNode<int>(5);
            var expectation = 3;
            root.Left = new BinaryTreeNode<int>(expectation);
            root.Right = new BinaryTreeNode<int>(7);
            Sut = new BinaryTreeEnumerator<int>(root);
            Sut.MoveNext();

            Sut.Current.ShouldBeEquivalentTo(expectation);
        }

        [Fact]
        public void InOrderTraversal_OnRightChildSetOnly_ShouldPickLeftChildFirst()
        {
            var root = new BinaryTreeNode<int>(5);
            var expectation = 3;
            root.Left = new BinaryTreeNode<int>(expectation);
            root.Right = new BinaryTreeNode<int>(7);
            Sut = new BinaryTreeEnumerator<int>(root);
            Sut.MoveNext();

            Sut.Current.ShouldBeEquivalentTo(expectation);
        }
    }
}
