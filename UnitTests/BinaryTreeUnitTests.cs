﻿using System;
using System.Collections.Generic;
using AlgosAndDataStructures;
using FluentAssertions;
using Xunit;

namespace UnitTests
{
    public class BinaryTreeUnitTests
    {
        public BinaryTree<int> Sut { get; set; }

        public BinaryTreeUnitTests()
        {
            Sut = new BinaryTree<int>();
        }

        [Fact]
        public void TreeShouldBeEnumerable()
        {
            Sut.Should().BeAssignableTo<IEnumerable<int>>();
        }

        [Fact]
        public void TreeShouldBeEmpty()
        {
            Sut.Should().BeEmpty();
        }

        [Fact]
        public void GetEnumeratorAfterConstructorShouldThrowInvalidOperationException()
        {
            var enumerator = Sut.GetEnumerator();
            Assert.Throws<InvalidOperationException>(() => enumerator.Current);
        }

        [Fact]
        public void GetEnumeratorReturnEnumeratorWithRootSetAsCurrent()
        {
            Sut.Root = new BinaryTreeNode<int>(5);

            var enumerator = Sut.GetEnumerator();
            enumerator.MoveNext();
            enumerator.Current.ShouldBeEquivalentTo(Sut.Root.Value);
        }

        [Fact]
        public void ManualEnumeratorUsageTreeShouldUseInOrderTraversal()
        {
            var enumerable = new[] { 4, 2, 1, 3, 6, 5, 7, 0, 1, 1 };
            Sut = new BinaryTree<int>(enumerable);

            var expectation = new[] { 0, 1, 1, 1, 2, 3, 4, 5, 6, 7 };

            var enumerator = Sut.GetEnumerator();
            var expEnumerator = expectation.GetEnumerator();
            for (int i = 0; i < 8; i++)
            {
                if (enumerator.MoveNext())
                {
                    var current = enumerator.Current;

                    expEnumerator.MoveNext();
                    var exp = expEnumerator.Current;
                    
                    current.ShouldBeEquivalentTo(exp);
                }
            }
        }

        [Fact]
        public void EnumeratorTreeShouldUseInOrderTraversal()
        {
            var enumerable = new[] { 4, 2, 1, 3, 6, 5, 7 };
            Sut = new BinaryTree<int>(enumerable);

            var expectation = new[] { 1, 2, 3, 4, 5, 6, 7 };

            Sut.Should().ContainInOrder(expectation);
        }

        [Fact]
        public void AddInternal_OnFirstItem_ShouldSetItAsRoot()
        {
            Sut.AddInternal(6);

            Sut.Root.Value.ShouldBeEquivalentTo(6);
        }

        [Fact]
        public void AddInternal_OnItemGreaterThanRoot_ShouldSetItAsRightChild()
        {
            Sut.AddInternal(4);
            Sut.AddInternal(6);

            Sut.Root.Right.Value.ShouldBeEquivalentTo(6);
        }

        [Fact]
        public void AddInternal_OnItemGreaterThanRight_ShouldSetItAsRightRightChild()
        {
            Sut.AddInternal(4);
            Sut.AddInternal(6);
            Sut.AddInternal(7);

            Sut.Root.Right.Right.Value.ShouldBeEquivalentTo(7);
        }

        [Fact]
        public void RootShouldBeNull()
        {
            Sut.Root.Should().BeNull();
        }
    }
}
