﻿using AlgosAndDataStructures;
using FluentAssertions;
using Xunit;

namespace UnitTests
{
    public class BinaryTreeNodeTests
    {
        public BinaryTreeNode<int> Sut { get; set; }

        public BinaryTreeNodeTests()
        {
            Sut = new BinaryTreeNode<int>(6);
        }

        [Fact]
        public void OnInitializationShouldNotHaveChildsSet()
        {
            Sut.Left.Should().BeNull();
            Sut.Right.Should().BeNull();
        }

        [Fact]
        public void ToStringShouldNotFail()
        {
            var dump = Sut.ToString();
            dump.Should().NotBeEmpty();
        }

        [Fact]
        public void ToStringWithChildsShouldNotFail()
        {
            Sut.Right = new BinaryTreeNode<int>(8);
            Sut.Right.Right = new BinaryTreeNode<int>(18);
            Sut.Right.Left = new BinaryTreeNode<int>(7);
            Sut.Right.Right.Right = new BinaryTreeNode<int>(128);
            var dump = Sut.Right.ToString();
            dump.ShouldBeEquivalentTo("7 <- (8) -> 18");
        }

        [Fact]
        public void OnInitializationWithDataShouldSetItsValue()
        {
            var expectation = 6;
            Sut = new BinaryTreeNode<int>(expectation);
            Sut.Value.ShouldBeEquivalentTo(expectation);
        }

        [Fact]
        public void SettingLeftChildShouldSetChildParent()
        {
            Sut.Left = new BinaryTreeNode<int>(4);
            Sut.Left.Parent.ShouldBeEquivalentTo(Sut);
        }

        [Fact]
        public void SettingRightChildShouldSetChildParent()
        {
            Sut.Right = new BinaryTreeNode<int>(4);
            Sut.Right.Parent.ShouldBeEquivalentTo(Sut);
        }

        [Fact]
        public void GetAncestor_OnHavingGrandParentAndParamTwo_ShouldReturnGrandParent()
        {
            Sut.Right = new BinaryTreeNode<int>(1);
            Sut.Right.Right = new BinaryTreeNode<int>(2);

            Sut.Right.Right.GetAncestor(2).ShouldBeEquivalentTo(Sut);
        }

        [Fact]
        public void GetAncestor_OnRootReached_ShouldReturnNull()
        {
            Sut.Right = new BinaryTreeNode<int>(1);
            Sut.Right.Right = new BinaryTreeNode<int>(2);

            Sut.Right.Right.GetAncestor(3).Should().BeNull();
        }
    }
}
