﻿using System.Text;

namespace AlgosAndDataStructures
{
    public class BinaryTreeNode<T>
    {
        private BinaryTreeNode<T> _left;
        private BinaryTreeNode<T> _right;
        
        public BinaryTreeNode(T data)
        {
            Value = data;
        }

        public T Value { get; set; }

        public BinaryTreeNode<T> Parent { get; private set; }

        public BinaryTreeNode<T> Left
        {
            get { return _left; }
            set
            {
                _left = value;
                _left.Parent = this;
            }
        }

        public BinaryTreeNode<T> Right
        {
            get { return _right; }
            set
            {
                _right = value;
                _right.Parent = this;
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (Left != null)
            {
                sb.AppendFormat("{0} <- ", Left.Value);
            }

            sb.AppendFormat("({0})", Value);

            if (Right != null)
            {
                sb.AppendFormat(" -> {0}", Right.Value);
            }

            return sb.ToString();
        }


        private int GetRightDepth(BinaryTreeNode<T> binaryTreeNode)
        {
            int rightDepth = 0;
            if (binaryTreeNode.Right != null)
            {
                rightDepth = 1 + GetDepth(binaryTreeNode.Right);
            }
            return rightDepth;
        }

        private int GetLeftDepth(BinaryTreeNode<T> binaryTreeNode)
        {
            int leftDepth = 0;
            if (binaryTreeNode.Left != null)
            {
                leftDepth = 1 + GetDepth(binaryTreeNode.Left);
            }
            return leftDepth;
        }

        private int GetDepth(BinaryTreeNode<T> binaryTreeNode)
        {
            var leftDepth = GetLeftDepth(binaryTreeNode);

            var rightDepth = GetRightDepth(binaryTreeNode);

            return leftDepth > rightDepth ? leftDepth : rightDepth;
        }

        public BinaryTreeNode<T> GetAncestor(int parentLevel)
        {
            if (parentLevel == 0)
                return this;

            if (Parent == null)
                return null;

            return Parent.GetAncestor(parentLevel - 1);
        }
    }
}