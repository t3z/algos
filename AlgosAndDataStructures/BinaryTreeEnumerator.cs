﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace AlgosAndDataStructures
{
    public class BinaryTreeEnumerator<T> : IEnumerator<T>
    {
        private bool _hasEnumerated;

        private readonly BinaryTreeNode<T> _root;

        private int _depth;

// ReSharper disable once InconsistentNaming
        internal BinaryTreeNode<T> current;

        internal TraversalMode TraversalMode { get; set; }

        public BinaryTreeEnumerator(TraversalMode traversalMode = TraversalMode.InOrder)
        {
            TraversalMode = traversalMode;
        }

        public BinaryTreeEnumerator(BinaryTreeNode<T> root, TraversalMode traversalMode = TraversalMode.InOrder)
            : this(traversalMode)
        {
            _root = root;
        }

        public void Dispose()
        {
        }

        public bool MoveNext()
        {
            switch (TraversalMode)
            {
                case TraversalMode.InOrder:
                    current = InOrderTraversal();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (current == null)
                _hasEnumerated = true;

            return current != null;
        }

        private BinaryTreeNode<T> InOrderTraversal()
        {
            if (current == null && !_hasEnumerated && _root != null)
            {
                return GetLeftMostNode(_root);
            }

            return GetNextInOrderTraversal(current);
        }

        internal BinaryTreeNode<T> GetNextInOrderTraversal(BinaryTreeNode<T> node)
        {
            // junk
            if (node == null)
                return null;

            // root
            if (node.Parent == null && node.Right == null)
                return null;

            // having right node? switch to it and start with the leftmost
            if (node.Right != null)
            {
                return GetLeftMostNode(node.Right);
            }

            // are we done? (reached initial right node) - go level Up
            if (node.Parent != null && node.Parent.Right == node)
            {
                if (_depth > 0)
                {
                    var d = _depth;
                    return node.GetAncestor(d);
                }

                _depth = 0;
                return node.Parent.Parent;
            }

            // we reached root Child
            if (node.Parent != null)
                return node.Parent;

            // hmmm
            return null;
        }

        private BinaryTreeNode<T> GetLeftMostNode(BinaryTreeNode<T> currentNode)
        {
            if (currentNode != null)
            {
                var left = currentNode.Left;
                if (left == null)
                {
                    return currentNode;
                }
                _depth++;
                return GetLeftMostNode(left);
            }
            return null;
        }

        public void Reset()
        {
            _hasEnumerated = false;
            _depth = 0;
        }

        public T Current
        {
            get
            {
                if (current == null && !_hasEnumerated && _root != null)
                {
                    throw new InvalidOperationException("Enumeration has not started. Call MoveNext.");
                }

                if (current == null)
                    throw new InvalidOperationException("Enumerator wasn't initialized.");

                return current.Value;
            }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }
    }
}