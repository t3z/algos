﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AlgosAndDataStructures
{
    public class BinaryTree<T> : IEnumerable<T>
        where T : IComparable<T>
    {
        public BinaryTree()
        {
        }

        public BinaryTree(IEnumerable<T> enumerable)
        {
            if (enumerable == null) return;

            var list = enumerable.ToList();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    AddInternal(item);
                }
            }
        }

        internal void AddInternal(T item)
        {
            if (Root == null)
            {
                Root = new BinaryTreeNode<T>(item);
                return;
            }

            AddToNode(Root, item);
        }

        private void AddToNode(BinaryTreeNode<T> root, T item)
        {
            // item is greater
            if (item.CompareTo(root.Value) >= 0)
            {
                if (root.Right == null)
                {
                    root.Right = new BinaryTreeNode<T>(item);
                }
                else
                    AddToNode(root.Right, item);
            }
            else
            {
                if (root.Left == null)
                {
                    root.Left = new BinaryTreeNode<T>(item);
                }
                else
                    AddToNode(root.Left, item);
            }
        }

        internal BinaryTreeNode<T> Root { get; set; }

        public IEnumerator<T> GetEnumerator()
        {
            var enumerator = new BinaryTreeEnumerator<T>(Root);
            return enumerator;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var item in this)
            {
                sb.AppendFormat("[{0}] ", item);
            }
            return sb.ToString();
        }
    }
}